﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chess.Data;
using UnityEngine.UI;
using Chess.View;

namespace Chess.Controller
{
    public class ReplayController : MonoBehaviour
    {
        [SerializeField] GameData gameData;
        [SerializeField] SerializationController serializationController;
        [SerializeField] ReplayView replayView;
        [SerializeField] RulesBrain rulesBrain;

        Coroutine replayCoroutine;

        void Awake() 
        {
            replayView.OnPlayReplayBtnClik += HandleOnPlayReplayBtnClick;
        }

        void Update()
        {
            
            if(Input.GetKeyDown(KeyCode.E))
            {            
                FastForwardReplayToEnd();
            }
            else if (Input.GetKeyDown(KeyCode.R))
            {           
                PlayReplay(); 
            }
        }

        public void PlayReplay()
        {
            if (gameData.IsPlayingReplay == false && (gameData.GameState == GameState.Playing || gameData.GameState == GameState.Check))
            {
                gameData.IsPlayingReplay = true;
                replayCoroutine = StartCoroutine(PlayReplayCO());
            }
        }

        void HandleOnPlayReplayBtnClick()
        {
            PlayReplay();
        }

        void FastForwardReplayToEnd()
        {
            if (gameData.IsPlayingReplay == true)
            {
                StopCoroutine(replayCoroutine);

                int lastIndex = gameData.MoveHistory.Count;
                gameData.BoardData.BoardGrid.CopyValues(gameData.MoveHistory[lastIndex - 1]);
                gameData.ApplyMetaDataFromHistory(lastIndex - 1);
                gameData.BoardData.TriggerBoardUpdate();
                gameData.IsPlayingReplay = false;
            }       
        }


        IEnumerator PlayReplayCO()
        {       
            int count = gameData.MoveHistory.Count;
            int i = 0;
            //int replayPlayer = 2;
            gameData.UpdateGameState(GameState.Playing); // hack to remove Check UI mark if there is one
            while (i < count)
            {
                gameData.SelectedSquare = new Vector2(-1,-1);
                gameData.BoardData.BoardGrid.CopyValues(gameData.MoveHistory[i]);
                 
                if(i == count - 1)
                {
                    gameData.ApplyMetaDataFromHistory(i);
                }
                else
                {
                    gameData.ApplyPlayerFromHistory(i);
                }
                    

                gameData.BoardData.TriggerBoardUpdate();
                i++;
                yield return new WaitForSeconds(1);
            }
            gameData.IsPlayingReplay = false;
        }
    }
}
