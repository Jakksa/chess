using UnityEngine;
using System.Collections.Generic;
using Chess.Data;
using Chess.View;
using System.Collections;

namespace Chess.Controller
{
    public class BoardController : MonoBehaviour 
    {
        [SerializeField] BoardView view;
        [SerializeField] GameData gameData;

        [SerializeField] MovementBrain movementBrain;
        [SerializeField] ActionBrain actionBrain;
        [SerializeField] RulesBrain rulesBrain;

        void Awake() 
        {
            view.OnSquareClick+= HandleSquareClick;
        }

        void Start() 
        {
            gameData.ResetToDefault();
            gameData.UpdateMoveHistory();
            gameData.UpdateMetaDataHistory();
        }

        void HandleSquareClick(SquareData data)
        {
            if(gameData.IsPlayingReplay == true || (gameData.GameState != GameState.Playing && gameData.GameState != GameState.Check))
                return;

            if(data.UnitType != UnitType.None)
            {
                HandleUnitClick(data);
            }
            else
            {
                ExecuteAction(data);
            }

        }

        void HandleUnitClick(SquareData data)
        {
            if (data.Player == gameData.CurrentPlayer) // select unit
            {
                if (data != gameData.GetSelectedSquare())
                {
                    gameData.SelectedSquare = new Vector2(data.X, data.Y);
                    UpdateMovement(data);                
                }
            }
            else 
            {
                ExecuteAction(data);
            }     
        }

        void UpdateMovement(SquareData data)
        {
            List<Vector2> list = movementBrain.GetMovement(data);
            actionBrain.UpdatePossibleActions(list, data);
        }

        void ExecuteAction(SquareData data)
        {
            if (data.PossibleAction == PossibleAction.None)
                    return;
                
            actionBrain.ExecuteAction(data);
            StartCoroutine(EndTurn());   
        }

        IEnumerator EndTurn()
        {
            while(CanEndTurn() == false)
            {
                yield return new WaitForSeconds(0.5f);             
            }

            gameData.BoardData.TriggerBoardUpdate();
            rulesBrain.CheckResetElPassan(gameData.OtherPlayer);
            gameData.UpdateMoveHistory();
            rulesBrain.EndTurnCheckRules(gameData.OtherPlayer);
            
            SwitchPlayer();
          
            gameData.UpdateMetaDataHistory();
    
        }

        bool CanEndTurn()
        {
            return gameData.BoardData.IsTherePawnToPromote() == false;
        }


        void SwitchPlayer()
        { 
            if (gameData.CurrentPlayer == 1)
                gameData.CurrentPlayer = 2;
            else
                gameData.CurrentPlayer = 1;
        }
    }
}