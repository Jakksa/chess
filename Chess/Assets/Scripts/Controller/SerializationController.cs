﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Chess.Data;
using Chess.View;

namespace Chess.Controller
{
    public class SerializationController : MonoBehaviour
    {
        [SerializeField] SerializationView view;
        [SerializeField] GameData gameData;
        [SerializeField] RulesBrain rulesBrain;

         void Awake() 
         {
            view.OnLoadButtonClick += HandleLoadButtonClick;
            view.OnSaveButtonClick += HandleSaveButtonClick;

            view.OnHideSaveUI += HandleHideSaveUI;
            view.OnHideLoadUI += HandleHideLoadUI;
            view.OnShowSaveUI += HandleShowSaveUI;
            view.OnShowLoadUI += HandleShowLoadUI;
   
        }

        void HandleSaveButtonClick(string fileName)
        {
            gameData.SaveJsonData(fileName + gameData.SaveFileExtension);
        }

        void HandleLoadButtonClick(string fileName)
        {
            gameData.LoadJsonData(fileName);
            gameData.ApplyLatestMetaData();
        }


        void HandleHideSaveUI(GameState previousGameState)
        {
            gameData.UpdateGameState(previousGameState);
        }

        void HandleHideLoadUI(GameState previousGameState, bool isGameLoaded)
        {
            Debug.Log("HandleHideLoadUI previous " + previousGameState);
            if(isGameLoaded == false)
                 gameData.UpdateGameState(previousGameState);
        }

        void HandleShowSaveUI()
        {
            gameData.UpdateGameState(GameState.Saving);
        }

        void HandleShowLoadUI()
        {
            gameData.UpdateGameState(GameState.Loading);
        }

    
    }
}
