﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chess.Data;
using Chess.View;
using System;

namespace Chess.Controller
{
    public class EndGameController : MonoBehaviour
    {
        [SerializeField] EndGameView view;
        [SerializeField] GameData gameData;
        [SerializeField] ReplayController replayController;

        void Awake() 
        {
            view.OnQuitClick += HandleQuitClick;
            view.OnReplayClick += HandleReplayClick;
            view.OnRestartClick += HandleRestartClick;
        }

        public void HandleRestartClick()
        {
            gameData.RestartGame();
        }

        public void HandleReplayClick()
        {
            replayController.PlayReplay();
        }

        public void HandleQuitClick()
        {
            Application.Quit();
        }
     
    }
}


