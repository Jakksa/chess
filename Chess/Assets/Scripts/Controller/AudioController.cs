﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chess.Data;

namespace Chess.Controller
{
    public class AudioController : MonoBehaviour
    {
        public float MusicVolume = 0.15f;
        public float SfxVolume = 0.5f;

        [SerializeField] GameData gameData;
        [SerializeField] AudioSource musicAudioSource;
        [SerializeField] AudioSource sfxAudioSource;

        [SerializeField] AudioClip checkAC;
        [SerializeField] AudioClip checkMateAC;
        [SerializeField] AudioClip drawAC;
        [SerializeField] AudioClip unitConsumedAC;
        [SerializeField] AudioClip unitMovedAC;
       
        void Awake()
        {
            gameData.OnGameStateChanged += HandleGameStatechange;
            gameData.OnUnitConsumed += HandleUnitConsumed;
            gameData.OnUnitMoved += HandleUnitMoved;
        }

        void HandleGameStatechange(GameState gameState)
        {
            if(gameState == GameState.Check)
            {
                sfxAudioSource.PlayOneShot(checkAC); 
            }
            else if(gameState == GameState.DrawTrippleFold || gameState == GameState.DrawLackOfMaterial || gameState == GameState.Stalemate )
            {
                sfxAudioSource.PlayOneShot(drawAC);
            }
            else if(gameState == GameState.CheckMate)
            {
                sfxAudioSource.PlayOneShot(checkMateAC);
            }
        }

        void HandleUnitConsumed()
        {
            sfxAudioSource.PlayOneShot(unitConsumedAC);
        }

        void HandleUnitMoved()
        {
            sfxAudioSource.PlayOneShot(unitMovedAC);
        }

        public void ToggleAudioOn()
        {
            musicAudioSource.volume = MusicVolume;
            sfxAudioSource.volume = SfxVolume;
        }

        public void ToggleAudioOff()
        {
            musicAudioSource.volume = 0;
            sfxAudioSource.volume = 0;
        }


    }
}
