﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chess.View;
using Chess.Data;

namespace  Chess.Controller
{
    public class PawnPromotionController : MonoBehaviour
    {
        [SerializeField] PawnPromotionView view;
        [SerializeField] RulesBrain rulesBrain;
        [SerializeField] GameData gameData;

        void Awake() 
        {
            view.OnPawnPromoted += HandlePawnPromotion;
            gameData.BoardData.OnPawnPromotion += UpdateGameState;
        }


        void HandlePawnPromotion(UnitData unitData)
        {
            gameData.UpdateGameState(GameState.Playing);
            rulesBrain.PromotePawn(unitData);
        }

        void UpdateGameState()
        {
            gameData.UpdateGameState(GameState.PawnPromotion);
        }
    } 
}

