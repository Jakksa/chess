﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chess.Data;
using UnityEngine.UI;
using System;

namespace Chess.View
{
    public class EndGameView : MonoBehaviour
    {
        [SerializeField] GameData gameData;
        [SerializeField] Canvas checkCanvas;
        [SerializeField] Canvas endGameCanvas;
        [SerializeField] Text endGameText;

        public Action OnRestartClick;
        public Action OnReplayClick;
        public Action OnQuitClick;

        void Awake() 
        {
            gameData.OnGameStateChanged += HandleGameStateChange;
        }

        void HandleGameStateChange(GameState newGameState)
        {
            if(newGameState == GameState.Check)
            {
                checkCanvas.enabled = true;
            }
            else
            {
                checkCanvas.enabled = false;
                endGameCanvas.enabled = true;
                switch(newGameState)
                {
                    case GameState.CheckMate:
                    {
                        string color = "White";          // should introduce player enum
                        if(gameData.CurrentPlayer == 2)
                            color = "Black";

                        endGameText.text = color + " player" + " WINS - CHECKMATE"; // should expose these not hardcode
                        break;
                    }
                    case GameState.DrawLackOfMaterial:
                    {
                        endGameText.text = " DRAW - LACK OF MATERIAL";
                        break;
                    }
                    case GameState.DrawTrippleFold:
                    {
                        endGameText.text = " DRAW - TRIPPLE FOLD";
                        break;
                    }
                    case GameState.Stalemate:
                    {
                        endGameText.text = " DRAW - STALEMATE";
                        break;
                    }
                    default:
                    {
                        endGameCanvas.enabled = false;
                        break;
                    }
                }
            }
        }

        public void HandleRestartClick()
        {
            OnRestartClick?.Invoke();
            endGameCanvas.enabled = false;
        }

        public void HandleReplayClick()
        {
            OnReplayClick?.Invoke();
            endGameCanvas.enabled = false;
        }

        public void HandleQuitClick()
        {
            OnQuitClick?.Invoke();
            endGameCanvas.enabled = false;
        }
    }
}
