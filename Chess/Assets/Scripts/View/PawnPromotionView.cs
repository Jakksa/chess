﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Chess.Data;
using System;

namespace Chess.View
{
    public class PawnPromotionView : MonoBehaviour
    {
        [SerializeField] Canvas pawnPromotionCanvas;
        [SerializeField] GameData gameData;

        public Image QueenImg;
        public Image KnightImg;
        public Image RookImg;
        public Image BishopImg;

        public Sprite QueenImgBlack;
        public Sprite KnightImgBlack;
        public Sprite RookImgBlack;
        public Sprite BishopImgBlack;

        public Sprite QueenImgWhite;
        public Sprite KnightImgWhite;
        public Sprite RookImgWhite;
        public Sprite BishopImgWhite;

        public Action<UnitData> OnPawnPromoted;


        void Awake() 
        {
            gameData.BoardData.OnPawnPromotion += ShowPawnPromotionUI;
        }

        public void Show(int player)
        {
            if(player == 2)
            {
                QueenImg.sprite = QueenImgBlack;
                KnightImg.sprite =KnightImgBlack;
                RookImg.sprite = RookImgBlack;
                BishopImg.sprite = BishopImgBlack;
            }
            else
            {
                QueenImg.sprite = QueenImgWhite;
                KnightImg.sprite = KnightImgWhite;
                RookImg.sprite = RookImgWhite;
                BishopImg.sprite = BishopImgWhite;
            }

            pawnPromotionCanvas.enabled = true;
        }

        public void Hide()
        {
            pawnPromotionCanvas.enabled = false;
        }

        void ShowPawnPromotionUI()
        {
            Show(gameData.CurrentPlayer);
        }

        void HidePawnPromotionUI()
        {
            Hide();
        }

        public void PromotePawn(UnitData newData)
        {
            OnPawnPromoted?.Invoke(newData);
            HidePawnPromotionUI();
        }
    }
}
