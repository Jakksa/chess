﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chess.Data;

namespace Chess.View
{
    public class SquareView : MonoBehaviour
    {
        public Action<SquareData> OnSquareClick;

        public void OnMouseDown()
        {
            OnSquareClick?.Invoke(GetComponent<SquareDataMono>().SquareRecord);
        }
    }
}