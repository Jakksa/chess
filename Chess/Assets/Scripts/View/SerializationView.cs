﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chess.Data;
using System;
using System.IO;
using UnityEngine.UI;

namespace Chess.View
{
    public class SerializationView : MonoBehaviour
    {
        [SerializeField] Canvas saveCanvas;
        [SerializeField] Canvas loadCanvas;
        [SerializeField] GameObject verticalLayoutGroup;
        [SerializeField] GameObject loadBtnPrefab;

        [SerializeField] GameData gameData;

        public Action<string> OnLoadButtonClick;
        public Action<string> OnSaveButtonClick;
        public Action<GameState> OnHideSaveUI;
        public Action<GameState, bool> OnHideLoadUI;
        public Action OnShowSaveUI;
        public Action OnShowLoadUI;

        GameState previousGameState;

        void Update() 
        {
            if(Input.GetKeyDown(KeyCode.S))
            {
                ShowSaveCanvas();
            }
            else if (Input.GetKeyDown(KeyCode.L))
            {
                ShowLoadCanvas();
            }
        }

        public void HandleLoadButtonClick(string text)
        {
            OnLoadButtonClick?.Invoke(text);
            HideLoadCavas(true);
        }

        public void HandleSaveButtonClick(Text inputText)
        {
            OnSaveButtonClick?.Invoke(inputText.text);
            HideSaveCanvas();
        }

        public void HandleCloseSaveBtnClick()
        {
            HideSaveCanvas();
        }

        public void HandleCloseLoadBtnClick()
        {
            HideLoadCavas(false);
        }

        public void HandleSaveInitBtnClick()
        {
            ShowSaveCanvas();
        }

        public void HandleLoadInitBtnClick()
        {
            ShowLoadCanvas();
        }


        
        void ShowSaveCanvas()
        {
            if(gameData.IsPlayingReplay == false && (gameData.GameState == GameState.Playing || gameData.GameState == GameState.Check))
            {
                previousGameState = gameData.GameState;
                OnShowSaveUI?.Invoke();
                saveCanvas.enabled = true;
            }
        }

        void ShowLoadCanvas()
        {
            if (gameData.IsPlayingReplay == false && (gameData.GameState == GameState.Playing || gameData.GameState == GameState.Check))
            {
                previousGameState = gameData.GameState;
                OnShowLoadUI?.Invoke();
                PopulateLoadCanvas();
                loadCanvas.enabled = true;
                           
            }
        }

        void HideSaveCanvas()
        {
            saveCanvas.enabled = false;
            OnHideSaveUI?.Invoke(previousGameState);
        }

        void HideLoadCavas(bool isGameLoaded)
        {
            loadCanvas.enabled = false;
            if(isGameLoaded == false)
                OnHideLoadUI?.Invoke(previousGameState, isGameLoaded);
        }

        void PopulateLoadCanvas()
        {
            DirectoryInfo dir = new DirectoryInfo(Application.persistentDataPath);
            FileInfo[] info = dir.GetFiles("*.*");

            foreach (Transform child in verticalLayoutGroup.transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            foreach (FileInfo f in info)
            {
                string fileName = "";
                if (f.ToString().Substring(f.ToString().Length - gameData.SaveFileExtension.Length) == gameData.SaveFileExtension)
                {
                    int lastIndex = f.ToString().LastIndexOf(Path.DirectorySeparatorChar);
                    fileName = f.ToString().Substring(lastIndex + 1);

                    Button btn = Instantiate(loadBtnPrefab, verticalLayoutGroup.transform).GetComponentInChildren<Button>();
                    Text btnText = btn.GetComponentInChildren<Text>();

                    btnText.text = fileName;
                    //Debug.Log(f.ToString());

                    btn.onClick.AddListener(delegate () { HandleLoadButtonClick(btnText.text); });
                }
            }
        }      
    }
}

