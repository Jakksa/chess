using UnityEngine;
using UnityEngine.UI;
using Chess.Data;
using System;

namespace Chess.View
{   
    public class ReplayView : MonoBehaviour 
    {
        [SerializeField] GameData gameData;
        public Canvas replayCanvas;
        public Button playReplayBtn;

        public Action OnPlayReplayBtnClik;

        void Awake() 
        {
            gameData.OnPlayingReplayChanged += HandleOnPlayingRepChanged;
        }

        void HandleOnPlayingRepChanged(bool isPlayingReplay)
        {
            if(isPlayingReplay)
            {
                playReplayBtn.gameObject.SetActive(false);
                replayCanvas.enabled = true;
            }            
            else
            {
                playReplayBtn.gameObject.SetActive(true);
                replayCanvas.enabled = false;
            }
                
        }

        public void HandlePlayReplayBtnClick()
        {
            if (gameData.IsPlayingReplay == false && (gameData.GameState == GameState.Playing || gameData.GameState == GameState.Check))
            {
                playReplayBtn.gameObject.SetActive(false);
                OnPlayReplayBtnClik?.Invoke();
            }
        }
        
    }
}