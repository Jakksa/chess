using System;
using UnityEngine;
using Chess.Data;
using UnityEngine.UI;

namespace Chess.View
{
    public class BoardView : MonoBehaviour 
    {
        public bool DoTurnCamera = false;
        [SerializeField] GameData gameData;
        [SerializeField] Color moveColor = new Color(0, 1, 1, 1);
        [SerializeField] Color attackColor = Color.red; 
        [SerializeField] Color selectedUnitColor = new Color(0, 1, 0, 1);
        [SerializeField] Text currentPlayerText;
        [SerializeField] Camera p1Camera;
        [SerializeField] Camera p2Camera;

        bool isBoardCreated = false;
        BoardGridData currentlyDisplayedBoardState;
        Vector2 currentlySelectedSquare;
        GameObject[,] squarePrefabs;

        public Action<SquareData> OnSquareClick;

        
        private void Awake() 
        {
            gameData.BoardData.OnBoardUpdated += UpdateBoard;
            gameData.OnCurrentPlayerChanged += UpdateCurrentPlayer;
            gameData.OnSelectedSquareChanged += UpdateSelectedUnit;
            gameData.BoardData.PossibleActionsUpdated += UpdateActionGridView;
            gameData.BoardData.OnBoardRestart += ResetBoard;
            currentlySelectedSquare = new Vector2(-1,-1);    
        }

        public void TurnCamSwitchOn()
        {
            DoTurnCamera = true;
            TurnCamera(gameData.CurrentPlayer);
        }

        public void TurnCamSwitchOff()
        {
            DoTurnCamera = false;
        }


        void SetupBoard()
        {
            currentlyDisplayedBoardState = new BoardGridData(gameData.BoardData.X, gameData.BoardData.Y); //this is empy board
            if(isBoardCreated == false)
                CreateBoard();

            ClearBoardUnits();
            BindCurrentBoardStateToSquares();
            UpdateBoard();
        }

        void CreateBoard()
        {
            squarePrefabs = new GameObject[gameData.BoardData.X, gameData.BoardData.Y];
            for (int i = 0; i < gameData.BoardData.X; i++)
            {
                for (int j = 0; j < gameData.BoardData.Y; j++)
                {
                    GameObject square = Instantiate(gameData.SquarePrefab, new Vector3(i, j, 0), Quaternion.identity, this.transform);
                    squarePrefabs[i, j] = square;
                    square.GetComponent<SquareView>().OnSquareClick += OnSquareObjectClick;

                    square.GetComponent<Renderer>().material.color = GetSquareColor(i, j);
                }
            }

            isBoardCreated = true;
        }

        void BindCurrentBoardStateToSquares()
        {
            for(int i = 0; i < gameData.BoardData.X; i++)
            {
                for (int j = 0; j < gameData.BoardData.Y; j++)
                {
                    GameObject squarePrefab = squarePrefabs[i, j];

                    SquareData squareData = gameData.GetSquareAt(i, j);
                    squarePrefab.GetComponent<SquareDataMono>().SquareRecord = squareData;
                }
            }
        }

        void ResetBoard()
        {
            SetupBoard();    
        }


        void UpdateBoard()
        {
            BoardGridData rd = gameData.BoardData.BoardGrid;
            for (int i = 0; i < gameData.BoardData.X; i++)
            {
                for (int j = 0; j < gameData.BoardData.Y; j++)
                {
                    if(rd.GetSquareAt(i, j).UnitType != currentlyDisplayedBoardState.GetSquareAt(i, j).UnitType ||
                    rd.GetSquareAt(i, j).Player != currentlyDisplayedBoardState.GetSquareAt(i, j).Player)
                    {
                        UpdateSquareUnit(new Vector2(i,j));
                    }
                }
            }

            currentlyDisplayedBoardState = new BoardGridData(gameData.BoardData.BoardGrid);
        }

        

        void UpdateSquareUnit(Vector2 coordinates)
        {
            int i = (int)coordinates.x;
            int j = (int)coordinates.y;
            SquareData d = gameData.GetSquareAt(i,j);
            if (squarePrefabs[i, j].transform.childCount > 0)
            {
                Destroy(squarePrefabs[i, j].transform.GetChild(0).gameObject);
            }

            if (d.UnitType != UnitType.None)
            {
                InstantiateUnitOnSquare(d);
            }
        }

        void InstantiateUnitOnSquare(SquareData d)
        {
            if (d.UnitType != UnitType.None)
            {
                GameObject unit = Instantiate(gameData.GetUnitPrefabByType(d.UnitType).gameObject, new Vector3(d.X, d.Y, 0.54f), gameData.GetUnitPrefabByType(d.UnitType).gameObject.transform.rotation, squarePrefabs[d.X, d.Y].transform);
                unit.GetComponent<Renderer>().material.color = gameData.GetUnitColorForPlayer(d.Player);
            }
        }

        void ClearBoardUnits()
        {
            for (int i = 0; i < gameData.BoardData.X; i++)
            {
                for (int j = 0; j < gameData.BoardData.Y; j++)
                {
                    if (squarePrefabs[i, j].transform.childCount > 0)
                    {
                        Destroy(squarePrefabs[i, j].transform.GetChild(0).gameObject);
                    }
                }
            }    
        }


        void UpdateActionGridView()
        {

            for (int i = 0; i < gameData.BoardData.X; i++)
            {
                for (int j = 0; j < gameData.BoardData.Y; j++)
                {

                    SquareData d = gameData.BoardData.BoardGrid.GetSquareAt(i,j);
                    GameObject squareInstance = squarePrefabs[i, j];
                    
                    if(d.PossibleAction == PossibleAction.Move || d.PossibleAction == PossibleAction.Castling)
                    {
                        squareInstance.GetComponent<Renderer>().material.color = moveColor;
                    }
                    else if(d.PossibleAction == PossibleAction.Consume || d.PossibleAction == PossibleAction.ElPassan)
                    { 
                        if(squareInstance.transform.childCount > 0)
                        {
                            squareInstance.GetComponent<Renderer>().material.color = moveColor;
                            squareInstance.transform.GetChild(0).GetComponent<Renderer>().material.color = attackColor;
                        }
                        else
                        {
                            squareInstance.GetComponent<Renderer>().material.color = attackColor;
                        }
                    }
                    else if(d.PossibleAction == PossibleAction.None)
                    {

                        squareInstance.GetComponent<Renderer>().material.color = GetSquareColor(i,j);
                        if (squareInstance.transform.childCount > 0)
                        {
                            Transform unit = squareInstance.transform.GetChild(0).transform;
                            unit.GetComponent<Renderer>().material.color = gameData.GetUnitColorForPlayer(d.Player);  
                        }

                    }

                    if(currentlySelectedSquare.x != -1)
                    {
                        //Debug.Log("currentlySelectedSquare!=null");
                        GameObject selectedSquareInstance = squarePrefabs[(int)currentlySelectedSquare.x, (int)currentlySelectedSquare.y];

                        if (selectedSquareInstance.transform.childCount > 0)
                        {
                            Transform unit = selectedSquareInstance.transform.GetChild(0).transform;
                            unit.GetComponent<Renderer>().material.color = selectedUnitColor;
                        } 
                    }

                }
            }
        } 


        void SelectUnit(Vector2 vect)
        {
            if(squarePrefabs[(int)vect.x, (int)vect.y].transform.childCount == 0)
            {
                Debug.LogError("TRYING TO SELECT WHEN THERE IS NO UNIT CHILD!!");
                return;
            }
            

            squarePrefabs[(int)vect.x, (int)vect.y].transform.GetChild(0).GetComponent<Renderer>().material.color = selectedUnitColor;
            currentlySelectedSquare = vect;        
        }

        void DeSelectUnit(Vector2 vect)
        {
            SquareData data = gameData.GetSquareAt(vect);
            if(data.UnitType != UnitType.None && squarePrefabs[data.X, data.Y].transform.childCount > 0 )
                squarePrefabs[data.X, data.Y].transform.GetChild(0).GetComponent<Renderer>().material.color = gameData.GetUnitColorForPlayer(data.Player);

            currentlySelectedSquare = new Vector2(-1,-1);
        }

        void UpdateCurrentPlayer(int Player)
        {
            if(Player == 1)
            {
                currentPlayerText.text = "Player: White";
                currentPlayerText.color = Color.white;
            }
            
            else
            {
                currentPlayerText.text = "Player: Black";
                currentPlayerText.color = Color.black;
            }
            

            TurnCamera(Player);
        }

        void UpdateSelectedUnit(Vector2 vect)
        {
            if (currentlySelectedSquare.x != -1)
                DeSelectUnit(currentlySelectedSquare);

            SquareData data = gameData.GetSelectedSquare();
            if(data == null)
            {
                currentlySelectedSquare = new Vector2(-1,-1);
                return;
            }
    
            SelectUnit(vect);
        }



        void OnSquareObjectClick(SquareData data)
        {
            OnSquareClick?.Invoke(data);
        }


        void TurnCamera(int player)
        {
            if(DoTurnCamera == false)
                return;

            if(player == 1)
            {
                p1Camera.enabled = true;
                p2Camera.enabled = false;
            }
            else
            {
                p2Camera.enabled = true;
                p1Camera.enabled = false;
            }
        
        }

        Color GetSquareColor(int x, int y)
        {
            if ((x + y) % 2 == 0) // za parne cno, za neparne bijelo
                return Color.black;
            else
                return Color.white;
        }

    }
}