﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess.Data
{
    public enum GameState
    {
        Playing,
        Saving,
        Loading,
        Check,
        CheckMate,
        DrawLackOfMaterial,
        DrawTrippleFold,
        Stalemate,
        PawnPromotion
    }
}
