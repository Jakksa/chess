﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess.Data
{
    public enum PossibleAction
    {
        None,
        Move,
        Consume,
        PawnPromotion,
        ElPassan,
        Castling
    }
}
