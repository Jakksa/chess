﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess.Data
{
    public enum MovementType
    {
        Standard,
        Jump,
        ConsumeOnly,
        Knight,
        MoveOnly
    }
}