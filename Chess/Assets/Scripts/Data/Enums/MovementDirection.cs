﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess.Data
{
    public enum MovementDirection
    {
        L,
        R,
        U,
        D,
        DL,
        DR,
        UL,
        UR
    }
}
