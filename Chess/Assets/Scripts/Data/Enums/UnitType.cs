﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess.Data
{
    public enum UnitType
    {
        None,
        Pawn,
        Rook,
        Bishop,
        Knight,
        Queen,
        King
    }

}
