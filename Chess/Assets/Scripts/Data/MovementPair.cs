﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess.Data
{
    [System.Serializable]
    public struct MovementPair
    {
        public MovementType MovementType;
        public MovementDirection Direction;
        public int Distance;
    }
}