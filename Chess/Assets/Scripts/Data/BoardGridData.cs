﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess.Data
{
    [System.Serializable]
    public class BoardGridData
    {

        [SerializeField] int x;
        [SerializeField] int y;

        [SerializeField] BoardRowData[] rowData;

        public BoardRowData[] RowData { get => rowData; set => rowData = value;}
        public int X { get => x; set => x = value; }
        public int Y { get => y; set => y = value; }

        public BoardGridData(int x, int y)
        {
            Initialize(x, y);
        }

        public BoardGridData(BoardGridData data)
        {
            Initialize(data.X, data.Y);
            CopyValues(data);
        }

        public void CopyValues(BoardGridData data)
        {
            for (int i = 0; i < X; i++)
            {
                for (int j = 0; j < Y; j++)
                {
                    RowData[i].Squares[j].CopyData(data.GetSquareAt(i, j));
                }
            }
        }

        public SquareData GetSquareAt(int x, int y)
        {
            if(x < 0 || y < 0)
                return null;
                
            return RowData[x].Squares[y];
        }

        public SquareData GetSquareAt(SquareData squareData)
        {
            Vector2 c = squareData.GetCoordinates();
            return RowData[(int)c.x].Squares[(int)c.y];
        }

        public void SetSquareAt(int x, int y, SquareData data)
        {
             RowData[x].Squares[y] = data;
        }

        public void CopyToSquareAt(int x, int y, SquareData data)
        {
            RowData[x].Squares[y].CopyData(data);
        }

        void Initialize(int x, int y)
        {
            this.X = x;
            this.Y = y;
            rowData = new BoardRowData[x];
            for (int i = 0; i < x; i++)
            {
                RowData[i] = new BoardRowData(y);
                for (int j = 0; j < y; j++)
                {
                    RowData[i].Squares[j] = new SquareData(i, j);
                }
            }
        }

        
    }

}