﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chess.Data;

namespace Chess.Data
{
    [System.Serializable]
    public class GameMetaData
    {
        [SerializeField] int currentPlayer = 1;
        [SerializeField] Vector2 player1ElPassanSquare;
        [SerializeField] Vector2 player2ElPassanSquare;
        [SerializeField] Vector2 selectedSquare;
        [SerializeField] GameState gameState;

        public GameMetaData(int player, Vector2 elP1, Vector2 elP2, Vector2 selectedS, GameState gs)
        {
            currentPlayer = player;

            player1ElPassanSquare = elP1;
            player2ElPassanSquare = elP2;
            selectedSquare = selectedS;

            gameState = gs;
        }

        public int CurrentPlayer { get => currentPlayer;}
        public Vector2 Player1ElPassanSquare { get => player1ElPassanSquare;}
        public Vector2 Player2ElPassanSquare { get => player2ElPassanSquare; }
        public Vector2 SelectedSquare { get => selectedSquare;}
        public GameState GameState { get => gameState;}
    }

}
