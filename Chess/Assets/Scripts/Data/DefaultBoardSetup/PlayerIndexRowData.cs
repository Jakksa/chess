﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess.Data
{
    [System.Serializable]
    public class PlayerIndexRowData
    {
        public int[] PlayerIndexes;
    }
}
