﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess.Data
{
    [System.Serializable]
    public class BoardRowData
    {
        public SquareData[] Squares;

        public BoardRowData(int x)
        {
            Squares = new SquareData[x];
        }
    }
}


