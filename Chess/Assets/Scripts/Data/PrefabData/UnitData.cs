using UnityEngine;

namespace Chess.Data
{
    [System.Serializable]
    public class UnitData : MonoBehaviour
    {
        [SerializeField] UnitMovement movement;
        [SerializeField] UnitType unitType;

        public UnitMovement Movement { get => movement; }
        public UnitType UnitType { get => unitType; }
        public Color OriginalColor { get => GetComponent<Renderer>().sharedMaterial.color; }

    }
}