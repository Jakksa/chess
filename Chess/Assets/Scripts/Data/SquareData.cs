﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess.Data
{
    [System.Serializable]
    public class SquareData
    {

        [SerializeField] int x = -1;
        [SerializeField] int y = -1;

        [SerializeField] bool isUnitMoved = true;
        [SerializeField] bool isElPassan = false;

        [SerializeField] UnitType unitType;
        [SerializeField] PossibleAction possibleAction;

        [SerializeField] int player;

        public UnitType UnitType { get => unitType; set => unitType = value; }
        //public Color OriginalColor { get => originalColor; set => originalColor = value; }
        public int Player { get => player; set => player = value; }
        public bool IsUnitMoved { get => isUnitMoved; set => isUnitMoved = value; }
        public int Y { get => y; }
        public int X { get => x; }
        public bool IsElPassan { get => isElPassan; set => isElPassan = value; }
        public PossibleAction PossibleAction { get => possibleAction; set => possibleAction = value; }

        public Vector2 GetCoordinates()
        {
            return new Vector2(X,Y);
        }

        public SquareData(int x, int y)
        {
            if (this.x != -1 && this.x != x)
                Debug.Log("POTENTIAL PROBLEM AT X " + x + " Y " + y);

            this.x = x;
            this.y = y;
            isUnitMoved = false;
            isElPassan = false;
            unitType = UnitType.None;
            possibleAction = PossibleAction.None;
            player = -1;
        }

        public SquareData(SquareData sd)
        {
            if(this.x != -1 && this.x != sd.x)
                Debug.Log("POTENTIAL PROBLEM AT X " + sd.x + " Y " + sd.y);

            this.x = sd.X;
            this.y = sd.Y;
            CopyData(sd);
        }

        public bool EqualTypeAndPlayer(SquareData sd)
        {
            if (sd.UnitType == UnitType && (sd.player == player || BothPlayersAreInvalid(sd.player, player)) )
                return true;
            else
                return false;
        }


        public void RemoveUnit()
        {
            unitType = UnitType.None;
            player = -1;
            isUnitMoved = true;
        }

        public void PlaceUnit(UnitType type, int player)
        {
            unitType = type;
            this.player = player;
        }

        public void CopyData(SquareData sd)
        {
            isElPassan = sd.IsElPassan;
            isUnitMoved = sd.IsUnitMoved;
            unitType = sd.UnitType;
            player = sd.player;
           // originalColor = sd.OriginalColor;
            possibleAction = sd.PossibleAction;
        }

        bool BothPlayersAreInvalid(int player1, int player2)
        {
            return (player1 != 1 && player1 != 2 && player2 != 1 && player2 != 2);
        }
    }
}
