using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;

namespace Chess.Data
{
    [CreateAssetMenu(fileName = "GameData", menuName = "Chess/GameData", order = 0)]
    public class GameData : ScriptableObject 
    {
        public string SaveFileExtension = ".save";
        [SerializeField] BoardData boardData;
        [SerializeField] Color Player1Color;
        [SerializeField] Color Player2Color;
        [SerializeField] GameObject squarePrefab;
        [SerializeField] List<UnitData> unitPrefabs;
        [SerializeField] UnitMovement blackPawnMovement;

        [SerializeField] List<BoardGridData> moveHistory;
        [SerializeField] List<GameMetaData> metaDataHistory;

        [SerializeField] List<BoardGridData> doubleFoldList;
        [SerializeField] List<BoardGridData> trippleFoldList;


        [SerializeField] int currentPlayer = 1;
        [SerializeField] Vector2 player1ElPassanSquare;
        [SerializeField] Vector2 player2ElPassanSquare;
        [SerializeField] Vector2 selectedSquare;
        [SerializeField] GameState gameState;
        
       
        [SerializeField]  bool isPlayingReplay = false;

        public Action<int> OnCurrentPlayerChanged;
        public Action<Vector2> OnSelectedSquareChanged;
        public Action<UnitData> OnSelectedUnitChanged;
        public Action<GameState> OnGameStateChanged;
        public Action<bool> OnPlayingReplayChanged;

        //public Action OnGameLoaded;
        public Action OnUnitConsumed;
        public Action OnUnitMoved;

        public int CurrentPlayer 
        { 
            get => currentPlayer; 
            set
            { 
                currentPlayer = value; OnCurrentPlayerChanged?.Invoke(currentPlayer);
            } 
        }

        public int OtherPlayer 
        {
            get{if (currentPlayer == 1)
                     return 2;
                 else 
                     return 1;
                }
        }

        public Vector2 SelectedSquare 
        { 
            get => selectedSquare; 
            set 
            { 
                selectedSquare = value; OnSelectedSquareChanged?.Invoke(selectedSquare);
            } 
        }

        public BoardData BoardData { get => boardData;}
        public GameObject SquarePrefab { get => squarePrefab;  }
        public GameState GameState { get => gameState; }
        public UnitMovement BlackPawnMovement { get => blackPawnMovement;}
        public Vector2 Player1ElPassanSquare { get => player1ElPassanSquare; set => player1ElPassanSquare = value; }
        public Vector2 Player2ElPassanSquare { get => player2ElPassanSquare; set => player2ElPassanSquare = value; }
        public List<BoardGridData> MoveHistory { get => moveHistory; }
        public bool IsPlayingReplay
         { 
            get => isPlayingReplay; 
            set
            {
                isPlayingReplay = value;
                OnPlayingReplayChanged?.Invoke(isPlayingReplay);
            }
         }


        public void ResetToDefault()
        { 
            CurrentPlayer = 1;
            moveHistory = new List<BoardGridData>();
            metaDataHistory = new List<GameMetaData>();
            doubleFoldList = new List<BoardGridData>();
            trippleFoldList = new List<BoardGridData>();

            player1ElPassanSquare = new Vector2(-1, -1);
            Player2ElPassanSquare = new Vector2(-1, -1);

            selectedSquare = new Vector2(-1,-1);
            UpdateGameState(GameState.Playing);
            boardData.ResetPawnPromotionCoordinates();
            boardData.RestartBoard();

            isPlayingReplay = false;
        }

        public void RestartGame()
        {
            ResetToDefault();
        }

        public SquareData GetSquareAt(int x, int y)
        {
            return BoardData.BoardGrid.GetSquareAt(x, y);
        }

        public SquareData GetSquareAt(float x, float y)
        {
            return GetSquareAt((int)x, (int)y);
        }

        public SquareData GetSquareAt(Vector2 v)
        {
            return GetSquareAt(v.x, v.y);
        }

        public UnitData GetUnitPrefabByType(UnitType type)
        {
            return unitPrefabs.Find(x => x.UnitType == type);
        }

        public SquareData GetSelectedSquare()
        {
            if(SelectedSquare.x == -1)
                return null;
            else
                return GetSquareAt(SelectedSquare);
        }

        public void ResetElPassanSquareForPlayer(int player)
        {
            if (player == 2 && Player2ElPassanSquare.x != -1)
            {
                //Debug.Log("Resseting el passan for player 2");
                GetSquareAt(Player2ElPassanSquare).IsElPassan = false;
                Player2ElPassanSquare = new Vector2( -1 , -1);
            }
            else if (player == 1 && Player1ElPassanSquare.x != -1)
            {
                //Debug.Log("Resseting el passan for player 1");
                GetSquareAt(Player1ElPassanSquare).IsElPassan = false;
                Player1ElPassanSquare = new Vector2(-1, -1);
            }
        }

        public UnitMovement GetUnitMovementByType(UnitType type)
        {
            return unitPrefabs.Find(x => x.UnitType == type).Movement;
        }

        public void UpdateGameState(GameState newState)
        {
            gameState = newState;
            //Debug.Log("NEW GAME STATE: " + newState);
            OnGameStateChanged?.Invoke(gameState);

        }

        public Color GetUnitColorForPlayer(int player)
        {
            Color color;
            if(player == 1)
                color = Player1Color;
            else
                color = Player2Color;

            return color;
        }


        public void UpdateMoveHistory()
        {
            if(ListContainsElement(doubleFoldList, boardData.BoardGrid))
            {
                //Debug.Log("ADDDING TO TRIPLE FOLD");
                trippleFoldList.Add(new BoardGridData(boardData.BoardGrid));
            }
            if (ListContainsElement(MoveHistory, boardData.BoardGrid))
            {
                //Debug.Log("ADDDING TO DOUBLE FOLD");
                doubleFoldList.Add(new BoardGridData(boardData.BoardGrid));
            }

            MoveHistory.Add(new BoardGridData(boardData.BoardGrid));
           
           // UpdateMetaDataHistory();
        }

        

        public bool AreThereAnyTrippleFolds()
        {
            return trippleFoldList.Count > 0;
        }
 

        public List<SquareData> GetUnitsForPlayer(int player)
        {
            List<SquareData> retList = new List<SquareData>();
            foreach(BoardRowData row in boardData.BoardGrid.RowData)
            {
                foreach(SquareData data in row.Squares)
                {
                    if(data.Player == player)
                        retList.Add(data);
                }
            }

            return retList;
        }

        public SquareData GetKingForPlayer(int player)
        {
            return boardData.GetKingForPlayer(player);
        }

        public void UpdateMetaDataHistory()
        {
            GameMetaData metadata = new GameMetaData(currentPlayer, player1ElPassanSquare, player2ElPassanSquare, selectedSquare, gameState);

            metaDataHistory.Add(metadata);
        }

        public void ApplyPlayerFromHistory(int index)
        {
            GameMetaData metaData = metaDataHistory[index];
            CurrentPlayer = metaData.CurrentPlayer;
        }

        public void ApplyLatestMetaData()
        {
            ApplyMetaDataFromHistory(metaDataHistory.Count - 1);
        }
        public void ApplyMetaDataFromHistory(int index)
        {
           // Debug.Log("ApplyMetaDataFromHistory");
            GameMetaData metaData = metaDataHistory[index];
            CurrentPlayer = metaData.CurrentPlayer;

            ResetElPassanSquareForPlayer(CurrentPlayer);
            Player1ElPassanSquare = metaData.Player1ElPassanSquare;
            Player2ElPassanSquare = metaData.Player2ElPassanSquare;   

            ResetElPassanSquareForPlayer(CurrentPlayer);
                
            SelectedSquare = new Vector2(-1,1);

            UpdateGameState(metaData.GameState);
        }

        public void SaveJsonData(string _fileName)
        {
            string _fullPath = Path.Combine(Application.persistentDataPath, _fileName);
            string _json = JsonUtility.ToJson(this);
            File.WriteAllText(_fullPath, _json);
            Debug.Log("<color=black>-Json- File Saved to: [" + _fullPath + "]</color>");
        }

        public GameData LoadJsonData(string _fileName)
        {
            boardData.BoardGrid.GetSquareAt(0,0);

            string _path = Path.Combine(Application.persistentDataPath, _fileName);
            if (!File.Exists(_path))
            {
                Debug.LogError("Not Found: [" + _path + "]");
                return default(GameData); //returns a null for reference type and 0 for values
            }

            // [START] DESERIALIZATION HACK TO PRESERVE CURRENT INSTANCE REFERENCES
            BoardData backupBoardData = boardData;
            List<UnitData> unitPrefabsBackup = unitPrefabs;
            GameObject squarePrefabBackup = squarePrefab;
            List<GameObject> goList = new List<GameObject>();
            for(int i = 0; i < unitPrefabsBackup.Count;i++)
            {
                goList.Add(unitPrefabsBackup[i].gameObject);
            }    
            UnitMovement blackPawnMovementBackup = blackPawnMovement;
            // [END] DESERIALIZATION HACK TO PRESERVE CURRENT INSTANCE REFERENCES

            GameData _deserializedData = this;
            JsonUtility.FromJsonOverwrite(File.ReadAllText(_path), _deserializedData);
            Debug.Log("-Json- File Loaded: " + _path);

            // [START] DESERIALIZATION HACK TO PRESERVE CURRENT INSTANCE REFERENCES
            boardData = backupBoardData;
            unitPrefabs = new List<UnitData>();
            for (int i = 0; i < goList.Count; i++)
            {
                unitPrefabs.Add(goList[i].GetComponent<UnitData>());
            }
            blackPawnMovement = blackPawnMovementBackup;
            squarePrefab = squarePrefabBackup;
            // [END] DESERIALIZATION HACK TO PRESERVE CURRENT INSTANCE REFERENCES
 
                
            boardData.BoardGrid.CopyValues(_deserializedData.moveHistory.Last());    
            boardData.TriggerBoardUpdate();

            return _deserializedData;
        }

        bool ListContainsElement(List<BoardGridData> list, BoardGridData bdata2)
        {
            foreach (BoardGridData bdata1 in list)
            {
                if (AreBoardsEqual(bdata1, bdata2))
                    return true;
            }

            return false;
        }

        bool AreBoardsEqual(BoardGridData board1, BoardGridData board2)
        {
            for (int i = 0; i < boardData.X; i++)
            {
                for (int j = 0; j < boardData.Y; j++)
                    if (board1.GetSquareAt(i, j).EqualTypeAndPlayer(board2.GetSquareAt(i, j)) == false)
                        return false;
            }

            return true;
        }


    }


    
}