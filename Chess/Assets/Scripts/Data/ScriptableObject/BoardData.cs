using UnityEngine;
using System.Collections.Generic;
using System;
using System.IO;

namespace Chess.Data
{
    [CreateAssetMenu(fileName = "BoardData", menuName = "Chess/BoardData", order = 1)]
    public class BoardData : ScriptableObject
    {
        [SerializeField] int x;
        [SerializeField] int y;

        [SerializeField] BoardGridData boardGrid;
        [SerializeField] UnitRowData[] unitGrid;
        [SerializeField] PlayerIndexRowData[] playerIndexesGrid;

        [SerializeField] Vector2 currentPawnPromotionCoordinates = new Vector2(-1,-1);

        public Action OnBoardUpdated;
        public Action OnPawnPromotion;
        public Action OnBoardRestart;
        public Action PossibleActionsUpdated;

        public UnitRowData[] UnitGrid { get => unitGrid; set => unitGrid = value; }
        public PlayerIndexRowData[] PlayerIndexesGrid { get => playerIndexesGrid; set => playerIndexesGrid = value; }
        public Vector2 CurrentPawnPromotionCoordinates { get => currentPawnPromotionCoordinates; }
        public BoardGridData BoardGrid
        {
            get { return boardGrid; }
            set { boardGrid = value; }
        }

        public int X { get => x;}
        public int Y { get => y; }

        public void RestartBoard()
        {
            boardGrid = new BoardGridData(x, y);

            for (int i = 0; i < X; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    SquareData d = new SquareData(i,j);

                    d.UnitType = unitGrid[i].Units[j];
                    d.Player = playerIndexesGrid[i].PlayerIndexes[j];
                    if(d.UnitType != UnitType.None)
                        d.IsUnitMoved = false;
                    else
                        d.IsUnitMoved = true;

                    boardGrid.RowData[i].Squares[j] = d;
                }
            }

            ResetPawnPromotionCoordinates();
            OnBoardRestart?.Invoke();
        }

        public bool IsTherePawnToPromote()
        {
            return currentPawnPromotionCoordinates.x != -1;
        }

        public void ResetPawnPromotionCoordinates()
        {
            currentPawnPromotionCoordinates = new Vector2(-1, -1);
        }

        public void SetPawnPromotionSquare(SquareData sd)
        {
            currentPawnPromotionCoordinates = sd.GetCoordinates();
            OnPawnPromotion?.Invoke();
        }

        public void RemoveUnitAtIndex(int x, int y)
        {
            boardGrid.GetSquareAt(x, y).RemoveUnit();

        }
        // we know that there is only one pawn promotion possible
        public void PromotePawn(UnitType type)
        {
            if(IsTherePawnToPromote())
            {
                SquareData sd = BoardGrid.GetSquareAt((int)currentPawnPromotionCoordinates.x, (int)currentPawnPromotionCoordinates.y);

                UpdateUnitTypeAtSquare(sd, type);
                ResetPawnPromotionCoordinates();
            }   
        }


        public void TriggerBoardUpdate()
        {
            OnBoardUpdated?.Invoke();
        }

        public void TriggerPossibleActionsUpdate()
        {
            PossibleActionsUpdated?.Invoke();
        }


        public void UpdateUnit(SquareData sd)
        {
            boardGrid.GetSquareAt(sd).PlaceUnit(sd.UnitType, sd.Player);
        }

        public SquareData GetKingForPlayer(int player)
        {
            for(int i = 0; i < X; i++)
            {
                for(int j = 0; j < y; j++)
                {
                    SquareData sd = boardGrid.GetSquareAt(i,j);
                    /*if (sd.UnitType == UnitType.King && sd.Player != player)
                    {
                        Debug.Log("FOUND KING FOR OTHER PLAYER" + sd.player);
                    }*/
                    if(sd.Player == player && sd.UnitType == UnitType.King)
                    {
                        return sd;
                    }
                }
            }

            Debug.LogError("CANNOT FIND KING");
            return null;
        }

  
        // just for view purposes
        public BoardGridData GetEmptyBoard()
        {
            return new BoardGridData(x,y);
        }

        public void SaveJsonData(string _fileName)
        {
            string _fullPath = Path.Combine(Application.persistentDataPath , _fileName);
            string _json = JsonUtility.ToJson(this);
            File.WriteAllText(_fullPath, _json);
            Debug.Log("<color=black>-Json- File Saved to: [" + _fullPath + "]</color>");
        }

        public BoardData LoadJsonData(string _fileName)
        {

            string _path = Path.Combine(Application.persistentDataPath, _fileName);
            if (!File.Exists(_path))
            {
                Debug.LogError("Not Found: [" + _path + "]");
                return default(BoardData); //returns a null for reference type and 0 for values
            }

            BoardData _deserializedData = this;
            JsonUtility.FromJsonOverwrite(File.ReadAllText(_path), _deserializedData);

            Debug.Log("-Json- File Loaded: " + _path);

            boardGrid = _deserializedData.boardGrid;
            TriggerBoardUpdate();
            return _deserializedData;
        }


        void UpdateUnitTypeAtSquare(SquareData sd, UnitType type)
        {
            sd.UnitType = type;
            sd.IsUnitMoved = true;
        }

    }
 }