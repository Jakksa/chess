using UnityEngine;
using System.Collections.Generic;

namespace Chess.Data
{
    [CreateAssetMenu(fileName = "UnitMovement", menuName = "Chess/UnitMovement", order = 3)]
    public class UnitMovement : ScriptableObject
    {
        [SerializeField] List<MovementPair> movementPairs;
        public List<MovementPair> MovementPairs { get => movementPairs; set => movementPairs = value; }
    }
}