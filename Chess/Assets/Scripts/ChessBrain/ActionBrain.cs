﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chess.Data;
using System;

namespace Chess.Controller
{   
    public class ActionBrain : MonoBehaviour
    {
        [SerializeField] GameData gameData;

        void Awake() 
        {
            gameData.OnSelectedSquareChanged += HandleSelectedUnitChanged;
        }

        public void ExecuteAction(SquareData data)
        {     
            if (data.PossibleAction == PossibleAction.None)
                return;

            if (data.PossibleAction == PossibleAction.Consume || data.PossibleAction == PossibleAction.Move)
            {
                if (gameData.GetSelectedSquare().UnitType == UnitType.Pawn)
                    CheckSetElPassan(gameData.GetSelectedSquare(), data);

                MoveUnitFromTo(gameData.GetSelectedSquare(), data);

                if(data.PossibleAction == PossibleAction.Consume)
                    gameData.OnUnitConsumed?.Invoke();
                else
                    gameData.OnUnitMoved?.Invoke();

            }
            else if (data.PossibleAction == PossibleAction.PawnPromotion)
            {
                //this is handled automaticaly, data fires event, view picks up etc.
            }
            else if (data.PossibleAction == PossibleAction.ElPassan)
            {
                MoveUnitFromTo(gameData.GetSelectedSquare(), data);
                RemovePawnBehindUnit(data);
            }
            else if (data.PossibleAction == PossibleAction.Castling)
            {
                MoveUnitFromTo(gameData.GetSelectedSquare(), data);
                MoveRook(data);
            }
            
            ResetActionGrid();
        }

        public void UpdatePossibleActions(List<Vector2> list, SquareData movingUnit)
        {
            foreach (Vector2 v in list)
            {
                UpdateSquarePossibleAction(v, movingUnit);
            }

            gameData.BoardData.TriggerPossibleActionsUpdate();
        }

        public void UpdateSquarePossibleAction(Vector2 v, SquareData movingUnit)
        {
            SquareData sd = gameData.BoardData.BoardGrid.GetSquareAt((int)v.x, (int)v.y);

            if (sd.UnitType != UnitType.None && sd.Player != gameData.CurrentPlayer)
            {
                gameData.BoardData.BoardGrid.GetSquareAt(sd.X, sd.Y).PossibleAction = PossibleAction.Consume;
            }
            else if (sd.UnitType == UnitType.None)
            {
                if (gameData.GetSelectedSquare().UnitType == UnitType.Pawn && sd.IsElPassan)
                    gameData.BoardData.BoardGrid.GetSquareAt(sd.X, sd.Y).PossibleAction = PossibleAction.ElPassan;
                else
                {
                    if (movingUnit.UnitType == UnitType.King && Math.Abs(sd.Y - movingUnit.Y) > 1)
                        gameData.BoardData.BoardGrid.GetSquareAt(sd.X, sd.Y).PossibleAction = PossibleAction.Castling;
                    else
                        gameData.BoardData.BoardGrid.GetSquareAt(sd.X, sd.Y).PossibleAction = PossibleAction.Move;
                }

            }
        }

        void CheckSetElPassan(SquareData source, SquareData dest)
        {
            if (Math.Abs(source.X - dest.X) > 1) // if pawn moves for two instead of one
            {
                if (gameData.CurrentPlayer == 1)
                {
                    gameData.BoardData.BoardGrid.GetSquareAt(dest.X - 1, dest.Y).IsElPassan = true;
                    gameData.Player1ElPassanSquare = new Vector2(dest.X - 1, dest.Y);
                }
                else
                {
                    gameData.BoardData.BoardGrid.GetSquareAt(dest.X + 1, dest.Y).IsElPassan = true;
                    gameData.Player2ElPassanSquare = new Vector2(dest.X + 1, dest.Y);
                }
            }
        }

        void MoveRook(SquareData kingData)
        {
            if (kingData.Y == 2)
            {
                // move right rook
                int rookx = kingData.X;
                int rooky = 0;
               
                int rookDestx = rookx;
                int rookDesty = 2 + 1;

                SquareData rookData = gameData.BoardData.BoardGrid.GetSquareAt(rookx, rooky);//gameData.BoardData.RemoveUnitAtIndex(rooky, rookx);
                SquareData rookDestination = gameData.BoardData.BoardGrid.GetSquareAt(rookDestx, rookDesty);
                MoveUnitFromTo(rookData, rookDestination);
            }
            else // data.x == 6
            {
                // move left rook
                int rookx = kingData.X;
                int rooky = 7;

                int rookDestx = rookx;
                int rookDesty = 6 - 1;

                SquareData rookData = gameData.BoardData.BoardGrid.GetSquareAt(rookx, rooky);//gameData.BoardData.RemoveUnitAtIndex(rooky, rookx);
                SquareData rookDestination = gameData.BoardData.BoardGrid.GetSquareAt(rookDestx, rookDesty);
                MoveUnitFromTo(rookData, rookDestination);
            }
        }

        void RemovePawnBehindUnit(SquareData data)
        {
            int x = data.X;
            int y = data.Y;
            if (data.Player == 1)
                x = data.X - 1;
            else
                x = data.X + 1;

            gameData.BoardData.RemoveUnitAtIndex(x, y);
        }

        void MoveUnitFromTo(SquareData source, SquareData destination)
        {
            destination.UnitType = source.UnitType;
            destination.Player = source.Player;

            CheckSetPawnPromotion(source, destination);

            source.RemoveUnit();
            if (source == gameData.GetSquareAt(gameData.SelectedSquare))
                gameData.GetSquareAt(gameData.SelectedSquare).UnitType = UnitType.None;



            gameData.BoardData.UpdateUnit(destination);
            gameData.BoardData.UpdateUnit(source);

            if (source == gameData.GetSquareAt(gameData.SelectedSquare))
                gameData.SelectedSquare = new Vector2(-1, -1);

            source.IsUnitMoved = true;
        }

        void CheckSetPawnPromotion(SquareData source, SquareData destination)
        {
            if(source.UnitType == UnitType.Pawn && (destination.X == 0 || destination.X == 7))
            {
                gameData.BoardData.SetPawnPromotionSquare(destination);
            }
        }


        void HandleSelectedUnitChanged(Vector2 vect)
        {
          //  Debug.Log("HandleSelectedUnitChanged");
            ResetActionGrid();
        }
        
        void ResetActionGrid()
        {
           // Debug.Log("ResetActionGrid");
            BoardRowData[] grid = gameData.BoardData.BoardGrid.RowData;
            for (int i = 0; i < grid.Length; i++)
            {
                for (int j = 0; j < grid[i].Squares.Length; j++)
                {
                    grid[i].Squares[j].PossibleAction = PossibleAction.None;
                }
            }

            gameData.BoardData.PossibleActionsUpdated();
        }

    }
}

