﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chess.Data;
using System;

namespace Chess.Controller
{
    public class MovementBrain : MonoBehaviour
    {
        [SerializeField] GameData gameData;
        [SerializeField] BasicMovementBrain basicBrain;
        [SerializeField] RulesBrain rulesBrain;

        public List<Vector2> GetMovement(SquareData data)
        {
            List<Vector2> retList = new List<Vector2>();
            retList = basicBrain.GetBasicMovement(data);

            if (data.UnitType == UnitType.King)
            {
                retList = rulesBrain.AdjustKingSquares(retList, data, GetBlockingSquaresForPlayer(data.Player));
            }

            /*if(rulesBrain.IsPlayerChecked(data.Player))
            {
                retList = RemoveMovesNotSolvingCheck(retList, data);
            }*/

            retList = RemoveMovesLeadingIntoSelfCheck(retList, data);
            return retList;
        }


        public List<Vector2> RemoveMovesLeadingIntoSelfCheck(List<Vector2> list, SquareData movingUnit)
        {
            List<Vector2> retList = new List<Vector2>();
            for (int i = 0; i < list.Count; i++)
            {
                SquareData sourceData = new SquareData(movingUnit);
                SquareData destinationData = new SquareData(gameData.GetSquareAt(list[i]));
                gameData.GetSquareAt(list[i]).CopyData(movingUnit);
                movingUnit.UnitType = UnitType.None;

                if(rulesBrain.IsPlayerChecked(movingUnit.Player) == false)
                {
                    retList.Add(list[i]);
                }

                movingUnit.UnitType = sourceData.UnitType;
                gameData.GetSquareAt(list[i]).CopyData(destinationData);
            }

            return retList;
        }


        public List<Vector2> GetBlockingSquaresForPlayer(int player)
        {
            List<Vector2> blockingSquares = new List<Vector2>();
            List<SquareData> units = gameData.GetUnitsForPlayer(player);
            foreach (SquareData sd in units)
            {
                basicBrain.GetBasicMovement(sd);
                blockingSquares.AddRange(basicBrain.GetLastBlockingSquares());
            }

            return blockingSquares;

        }

        public List<Vector2> GetSquaresThreateningKingForPlayer(int player)
        {
            List<Vector2> kingThreatSquares = new List<Vector2>();
            List<SquareData> enemyUnits = gameData.GetUnitsForPlayer(rulesBrain.GetOtherPlayer(player));
            foreach (SquareData sd in enemyUnits)
            {
                kingThreatSquares.AddRange(basicBrain.GetBasicMovement(sd, true));
            }

            return kingThreatSquares;

        }

        List<Vector2> RemoveMovesNotSolvingCheck(List<Vector2> list, SquareData data)
        {
            if(data.UnitType == UnitType.King) // moving king automaticaly solves check, king related check is done elsewhere
                return list;

            List<Vector2> potentialKingThreatSquares;     
            potentialKingThreatSquares = GetSquaresThreateningKingForPlayer(data.Player);

            List<Vector2> retList = new List<Vector2>();
            for (int i = 0; i < list.Count; i++)
            {

                if (potentialKingThreatSquares == null)
                {
                    Debug.LogError("potentialKingThreatSquares IS NULL!!! Maybe implement logic to get this here");
                }
                else if (potentialKingThreatSquares.Contains(list[i]))
                {
                    retList.Add(list[i]);
                }
            }

            return retList;
        }

      
    }
}
