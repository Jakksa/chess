﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chess.Data;
using System;
using UnityEngine.UI;

namespace Chess.Controller
{
    public class RulesBrain : MonoBehaviour
    {
        [SerializeField] GameData gameData;
        [SerializeField] BasicMovementBrain basicBrain;
        [SerializeField] MovementBrain movementBrain;

        public void EndTurnCheckRules(int player)
        {    
            //Debug.Log("CHECKING IS PLAYER IN CHECK MATE FOR PLAYER " + player);
            if (IsLackOfMaterial() || IsThreeFoldRule())
            {
                if (IsThreeFoldRule())
                    gameData.UpdateGameState(GameState.DrawTrippleFold);
                else if(IsLackOfMaterial())
                    gameData.UpdateGameState(GameState.DrawLackOfMaterial);                
            }
            else if (IsPlayerChecked(player))
            {

                if (IsCheckMate(player))
                {
                    gameData.UpdateGameState(GameState.CheckMate);
            
                }
                else
                {
                    gameData.UpdateGameState(GameState.Check);
                }
                
            }
            else if(IsStaleMate())
            {
                gameData.UpdateGameState(GameState.Stalemate);
            }
            else
            {
                gameData.UpdateGameState(GameState.Playing);
            }
        

            // TODO: Is50MovesRule(gameData.OtherPlayer);
        }

        public void CheckResetElPassan(int player)
        {
           gameData.ResetElPassanSquareForPlayer(player);
        }

        public bool IsPlayerChecked(int player) // if players king is in path of any enemy unit
        {
            List<SquareData> list = GetUnitsAttackingKing(player);
            if (list.Count > 0)
            {
                return true;
            }

            return false;
        }

        public void PromotePawn(UnitData data)
        {
            gameData.BoardData.PromotePawn(data.UnitType);
        }

        public List<Vector2> AdjustKingSquares(List<Vector2> list, SquareData kingData, List<Vector2> blockingSquares)
        {
            List<Vector2> castling = TryGetCastlingSquares(kingData);
            list.AddRange(castling);
            list = RemoveCheckSquares(list, blockingSquares, kingData.Player);

            return list;
        }

        public int GetOtherPlayer(int player)
        {
            if (player == 1)
                return 2;
            else
                return 1;
        }



        bool IsThreeFoldRule()
        {
            return gameData.AreThereAnyTrippleFolds();
        }

        bool IsLackOfMaterial()
        {
            List<SquareData> player1Squares = gameData.GetUnitsForPlayer(1);
            List<SquareData> player2Squares = gameData.GetUnitsForPlayer(2);
            if(player1Squares.Count + player2Squares.Count == 3)
            {
                foreach(SquareData sd in player1Squares)
                {
                    if(sd.UnitType == UnitType.Knight || sd.UnitType == UnitType.Bishop)
                    {
                        Debug.Log("Knight or Bishop");
                        return true;
                    }
                }

                foreach (SquareData sd in player2Squares)
                {
                    if (sd.UnitType == UnitType.Knight || sd.UnitType == UnitType.Bishop)
                    {
                        Debug.Log("Knight or Bishop");
                        return true;
                    }
                }
               
            }
            if(player1Squares.Count + player2Squares.Count == 2)
            {
                Debug.Log("ONLY TWO KINGS");
                return true;
            }

            return false;
        }

        bool IsStaleMate()
        {
            List<SquareData> otherPlayerSquares = gameData.GetUnitsForPlayer(gameData.OtherPlayer);
            foreach (SquareData sd in otherPlayerSquares)
            {
                List<Vector2> list1 = movementBrain.GetMovement(sd);
                if (list1.Count > 0)
                    return false;
            }


            return true;
        }

        List<SquareData> GetUnitsAttackingKing(int player) // if players king is on movement path of any enemy unit
        {
            List<SquareData> retList = new List<SquareData>();
            SquareData king = gameData.GetKingForPlayer(player);

            List<SquareData> attackingPlayerUnits = gameData.GetUnitsForPlayer(GetOtherPlayer(player));
            foreach (SquareData sd in attackingPlayerUnits)
            {
                List<Vector2> list = basicBrain.GetBasicMovement(sd); //special movement cannnot give check

                foreach (Vector2 v in list)
                {
                    if (king.X == v.x && king.Y == v.y)
                        retList.Add(sd);
                }
            }

            return retList;
        }


        bool IsCheckMate(int player)
        {
            List<SquareData> enemyUnits = GetUnitsAttackingKing(player);
            if (enemyUnits.Count > 0) // this is basically if check check
            {
                SquareData king = gameData.GetKingForPlayer(player);

                List<Vector2> basicKingMovements = basicBrain.GetBasicMovement(king);
                List<Vector2> kingMovements = RemoveCheckSquares(basicKingMovements, movementBrain.GetBlockingSquaresForPlayer(GetOtherPlayer(player)), player);
                if (kingMovements.Count > 0) //if king can move, its not check mate
                    return false;
                else
                {
                    if (enemyUnits.Count == 1) //more than one threat cannot be removed, 
                    {
                        Debug.Log("INITIATING KING THREAD SQUARES CALCULATION FOR ");
                        basicBrain.GetBasicMovement(enemyUnits[0], true);
                        List<Vector2> attackingSquares = movementBrain.GetSquaresThreateningKingForPlayer(player);
                        List<SquareData> units = gameData.GetUnitsForPlayer(player);
                        foreach (SquareData sd in units) //if any unit can remove threat, while not opening new check, then it is not cm
                        {
                            List<Vector2> list = basicBrain.GetBasicMovement(sd);
                            list = movementBrain.RemoveMovesLeadingIntoSelfCheck(list, sd);

                            foreach (Vector2 v in attackingSquares)
                            {
                                if (list.Contains(v))
                                    return false;
                            }
                        }
                    }
                }
            }

            return true;
        }

        bool Is50MovesRule(int player)
        {
            return false;
        }


        List<Vector2> RemoveCheckSquares(List<Vector2> list, List<Vector2> blockingSquares, int player)
        {
            List<Vector2> retList = new List<Vector2>();
            for (int i = 0; i < list.Count; i++)
            {
                if (IsSquareUnderAttack((int)list[i].x, (int)list[i].y, player) == false && blockingSquares.Contains(list[i]) == false)
                {
                    retList.Add(list[i]);
                }
            }
            return retList;
        }

        List<Vector2> TryGetCastlingSquares(SquareData kingSquare)
        {
            List<Vector2> retList = new List<Vector2>();

            if (kingSquare.IsUnitMoved == false)
            {
                SquareData leftRook;
                leftRook = gameData.BoardData.BoardGrid.GetSquareAt(kingSquare.X, 0);
                if (leftRook.IsUnitMoved == false)
                {
                    if (IsQueenSideClear(kingSquare))
                        retList.Add(new Vector2(kingSquare.X, kingSquare.Y - 2));   
                }

                SquareData rightRook;
                rightRook = gameData.BoardData.BoardGrid.GetSquareAt(kingSquare.X, 7);
                if (rightRook.IsUnitMoved == false)
                {
                    if (IsKingSideClear(kingSquare))

                        retList.Add(new Vector2(kingSquare.X, kingSquare.Y + 2));
                }
            }

            return retList;
        }

        bool IsQueenSideClear(SquareData king)
        {
            for (int i = 1; i < king.Y; i++)
            {
                SquareData sd = gameData.BoardData.BoardGrid.GetSquareAt(king.X, i);
                if (sd.UnitType != UnitType.None || IsSquareUnderAttack(sd.X, sd.Y, king.Player))
                    return false;

            }
            return true;
        }


        bool IsKingSideClear(SquareData king)
        {
            for (int i = king.Y + 1; i < gameData.BoardData.X - 1; i++)
            {
                SquareData sd = gameData.BoardData.BoardGrid.GetSquareAt(king.X, i);
                if (sd.UnitType != UnitType.None || IsSquareUnderAttack(sd.X, sd.Y, king.Player))
                    return false;
            }
            return true;
        }

        bool IsSquareUnderAttack(int x, int y, int player)
        {
            SquareData sdCheck = gameData.BoardData.BoardGrid.GetSquareAt(x, y);

            List<SquareData> opposingPlayerUnits = gameData.GetUnitsForPlayer(GetOtherPlayer(player));
            foreach (SquareData sd in opposingPlayerUnits)
            {
                List<Vector2> list = basicBrain.GetAttackMovement(sd);
                 //List<Vector2> list = basicBrain.GetBasicMovement(sd);
                foreach (Vector2 v in list)
                {
                    if (sdCheck.X == v.x && sdCheck.Y == v.y)
                        return true;
                }
            }

            return false;
        }

    }
}
