﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chess.Data;
using System;

namespace Chess.Controller
{
    public class BasicMovementBrain : MonoBehaviour
    {
        [SerializeField] GameData gameData;

        SquareData currentUnit;
        int currentStartingX;
        int currentStartingY;

        int maxX;
        int maxY;

        List<Vector2> lastBlockingSquares;
        List<Vector2> potentialKingAttackingSquares;

        bool doCalculateKingAttackSquares;

        void Start() 
        {
            maxX = gameData.BoardData.X;
            maxY = gameData.BoardData.Y;
        }

        public List<Vector2> GetLastBlockingSquares()
        {
            return lastBlockingSquares;
        }

        public List<Vector2> GetAttackMovement(SquareData data)
        {
            return GetBasicMovement(data, false, true);
        }

        public List<Vector2> GetBasicMovement(SquareData data, bool doCalculateKingAttackSquares = false, bool isCalculatingAttackSquares = false)
        {
            lastBlockingSquares = new List<Vector2>();
            this.doCalculateKingAttackSquares = doCalculateKingAttackSquares;
            currentUnit = data;
            List<Vector2> retList = new List<Vector2>();
            UnitMovement movement = gameData.GetUnitMovementByType(data.UnitType);
            if (data.UnitType == UnitType.Pawn && data.Player == 2)
                movement = gameData.BlackPawnMovement;

            for (int i = 0; i < movement.MovementPairs.Count; i++)
            {
                potentialKingAttackingSquares = new List<Vector2>();
                MovementPair pair = movement.MovementPairs[i];
                if (data.UnitType == UnitType.Pawn && pair.MovementType == MovementType.MoveOnly)
                {
                    if (data.IsUnitMoved == false)
                        pair.Distance = 2;
                    else
                        pair.Distance = 1;
                }

                int x = data.X;
                int y = data.Y;
                currentStartingX = x;
                currentStartingY = y;
                if (pair.MovementType == MovementType.Standard || pair.MovementType == MovementType.ConsumeOnly || pair.MovementType == MovementType.MoveOnly)
                {
                    while (CanContinue(x, y, pair))
                    {
                        Vector2 vect = GetNextSquareCoordinates(x, y, pair);
                        x = (int)vect.x;
                        y = (int)vect.y;
                        if (IsBlocking(x, y))
                        {
                            lastBlockingSquares.Add(vect);
                            //Debug.Log("BREAK " + x + " " + y);
                            break;
                        }
                        else
                        {
                            if (pair.MovementType == MovementType.MoveOnly)
                            {
                                if (IsConsumePossible(x, y, data.UnitType == UnitType.Pawn))
                                    break;

                                if(isCalculatingAttackSquares && data.UnitType == UnitType.Pawn) // Pawn is the only unit that cannot attack the way it moves
                                    break;

                                retList.Add(new Vector2(x, y));
                                potentialKingAttackingSquares.Add(new Vector2(x, y));
                            }
                            else if (pair.MovementType == MovementType.ConsumeOnly)
                            {
                                if (IsConsumePossible(x, y, data.UnitType == UnitType.Pawn) || isCalculatingAttackSquares)
                                {
                                    //Debug.Log("Adding " + x + " " + y);
                                    retList.Add(new Vector2(x, y));
                                    potentialKingAttackingSquares.Add(new Vector2(x, y));
                                    break;
                                }
                            }
                            else
                            {
                                //Debug.Log("Adding " + x + " " + y);
                                retList.Add(new Vector2(x, y));
                                potentialKingAttackingSquares.Add(new Vector2(x, y));
                                if (IsConsumePossible(x, y, data.UnitType == UnitType.Pawn))
                                    break;
                            }

                        }
                    }
                }
                else if (pair.MovementType == MovementType.Jump)
                {
                    Vector2 vect = GetJumpDestination(x, y, pair);
                    if (vect.x != -1)
                        retList.Add(vect);
                }
                else if (pair.MovementType == MovementType.Knight)
                {
                    retList.AddRange(GetKnightDestinations(x, y, pair));
                }
            }

            currentUnit = null;

            if(doCalculateKingAttackSquares)
                return potentialKingAttackingSquares;

            return retList;
        }



        List<Vector2> GetKnightDestinations(int x, int y, MovementPair pair)
        {
            List<Vector2> retList = new List<Vector2>();

            Vector2 destination = GetJumpDestination(x, y, pair, true);
            if (destination.x == -1)
                return retList;

            MovementPair p = new MovementPair();
            p.Distance = 1;
            p.MovementType = MovementType.Jump;
            Vector2 nextJump = new Vector2(-1, -1);
            if (pair.Direction == MovementDirection.U || pair.Direction == MovementDirection.D)
            {
                p.Direction = MovementDirection.L;
                nextJump = GetJumpDestination((int)destination.x, (int)destination.y, p);
                if (nextJump.x != -1)
                    retList.Add(nextJump);


                p.Direction = MovementDirection.R;
                nextJump = GetJumpDestination((int)destination.x, (int)destination.y, p);
                if (nextJump.x != -1)
                    retList.Add(nextJump);

            }
            else
            {
                p.Direction = MovementDirection.U;
                nextJump = GetJumpDestination((int)destination.x, (int)destination.y, p);
                if (nextJump.x != -1)
                    retList.Add(nextJump);


                p.Direction = MovementDirection.D;
                nextJump = GetJumpDestination((int)destination.x, (int)destination.y, p);
                if (nextJump.x != -1)
                    retList.Add(nextJump);

            }


            return retList;
        }

        Vector2 GetNextSquareCoordinates(int x, int y, MovementPair pair)
        {
            Vector2 retVect = new Vector2(-1, -1);

            int newx = x;
            int newy = y;

            switch (pair.Direction)
            {
                case MovementDirection.U:
                    {
                        newy = y + 1;
                        break;
                    }
                case MovementDirection.D:
                    {
                        newy = y - 1;
                        break;
                    }
                case MovementDirection.L:
                    {
                        newx = x + 1;
                        break;
                    }
                case MovementDirection.R:
                    {
                        newx = x - 1;
                        break;
                    }
                case MovementDirection.UL:
                    {
                        newy = y + 1;
                        newx = x + 1;
                        break;
                    }
                case MovementDirection.UR:
                    {
                        newy = y + 1;
                        newx = x - 1;
                        break;
                    }
                case MovementDirection.DL:
                    {
                        newy = y - 1;
                        newx = x + 1;
                        break;
                    }
                case MovementDirection.DR:
                    {
                        newy = y - 1;
                        newx = x - 1;
                        break;
                    }

            }

            if (newx < gameData.BoardData.X && newx >= 0 && newy < gameData.BoardData.Y && newy >= 0)
            {
                retVect = new Vector2(newx, newy);
            }

            return retVect;

        }

        bool CanContinue(int x, int y, MovementPair pair)
        {
            switch (pair.Direction)
            {
                case MovementDirection.U:
                    {
                        if (pair.Distance == -1)
                            return y < maxY - 1;
                        else
                            return y < currentStartingY + pair.Distance && y < maxY - 1;
                    }
                case MovementDirection.D:
                    {
                        if (pair.Distance == -1)
                            return y > 0;
                        else
                            return y > 0 && y > currentStartingY - pair.Distance;
                    }
                case MovementDirection.L:
                    {
                        if (pair.Distance == -1)
                            return x < maxX - 1;
                        else
                            return x < maxX - 1 && x < currentStartingX + pair.Distance;

                    }
                case MovementDirection.R:
                    {
                        if (pair.Distance == -1)
                            return x > 0;
                        else
                            return x > 0 && x > currentStartingX - pair.Distance;
                    }
                case MovementDirection.UL:
                    {
                        if (pair.Distance == -1)
                            return (y < maxY - 1 && x < maxX - 1);
                        else
                            return (y < maxY - 1 && x < maxX - 1 && y < currentStartingY + pair.Distance && x < currentStartingX + pair.Distance);
                    }
                case MovementDirection.UR:
                    {
                        if (pair.Distance == -1)
                            return (y < maxY - 1 && x > 0);
                        else
                            return (y < maxY - 1 && x > 0 && y < currentStartingY + pair.Distance && x > currentStartingX - pair.Distance);
                    }
                case MovementDirection.DL:
                    {
                        if (pair.Distance == -1)
                            return (y > 0 && x < maxX - 1);
                        else
                            return (y > 0 && x < maxX - 1 && y > currentStartingY - pair.Distance && x < currentStartingX + pair.Distance);
                    }
                case MovementDirection.DR:
                    {
                        if (pair.Distance == -1)
                            return (y > 0 && x > 0);
                        else
                            return (y > 0 && x > 0 && y > currentStartingY - pair.Distance && x > currentStartingX - pair.Distance);
                    }
                default:
                    {
                        Debug.LogError("THIS SHOULD NEVER HAPPEN!");
                        return false;

                    }
            }
        }

        Vector2 GetJumpDestination(int x, int y, MovementPair pair, bool isFirstKnightJump = false)
        {
            Vector2 retVect = new Vector2(-1, -1);
            int newx = x;
            int newy = y;

            switch (pair.Direction)
            {
                case MovementDirection.U:
                    {
                        if (pair.Distance == -1)
                        {
                            newy = gameData.BoardData.Y;
                        }
                        else
                        {
                            newy = y + pair.Distance;
                        }
                        break;
                    }
                case MovementDirection.D:
                    {
                        if (pair.Distance == -1)
                        {
                            newy = 0;
                        }
                        else
                        {
                            newy = y - pair.Distance;
                        }
                        break;
                    }
                case MovementDirection.L:
                    {
                        if (pair.Distance == -1)
                        {
                            newx = gameData.BoardData.X;
                        }
                        else
                        {
                            newx = x + pair.Distance;
                        }
                        break;
                    }
                case MovementDirection.R:
                    {
                        if (pair.Distance == -1)
                        {
                            newx = 0;
                        }
                        else
                        {
                            newx = x - pair.Distance;
                        }
                        break;
                    }
                    /*
                     Other directions, not needed for classic chess
                    }*/
            }

            if (newx < gameData.BoardData.X && newx >= 0 && newy < gameData.BoardData.Y && newy >= 0 && (isFirstKnightJump || IsBlocking(newx, newy) == false))
            {
                retVect = new Vector2(newx, newy);
            }
            return retVect;
        }

        bool IsThereUnitAtSquare(int x, int y)
        {
            SquareData sd = gameData.BoardData.BoardGrid.GetSquareAt(x, y);
            if (sd.UnitType != UnitType.None)
            {
                return true;
            }
            else
                return false;

        }

        bool IsConsumePossible(int x, int y, bool isPawn = false)
        {
            SquareData targetSquare = gameData.BoardData.BoardGrid.GetSquareAt(x, y);
            if (isPawn && targetSquare.UnitType == UnitType.None && targetSquare.IsElPassan)
            {
                return true;
            }

            if (targetSquare.UnitType != UnitType.None && targetSquare.Player != currentUnit.Player)
            {
                if (targetSquare.UnitType == UnitType.King && doCalculateKingAttackSquares)
                {
                    for (int i = 0; i < potentialKingAttackingSquares.Count; i++)
                    {
                        if (potentialKingAttackingSquares[i].x == targetSquare.X && potentialKingAttackingSquares[i].y == targetSquare.Y)
                        {
                            potentialKingAttackingSquares.RemoveAt(i);
                            break;
                        }
                    }
                    potentialKingAttackingSquares.Add(new Vector2(currentUnit.X, currentUnit.Y));

                    //gameData.AddKingThreatSquaresForPlayer(targetSquare.Player, potentialKingAttackingSquares);
                }
                return true;
            }
            else
                return false;
        }

        bool IsBlocking(int x, int y)
        {
            //Debug.Log("Acessing " + x + " " + y);
            SquareData sd = gameData.BoardData.BoardGrid.GetSquareAt(x, y);
            return (sd.UnitType != UnitType.None && sd.Player == currentUnit.Player);
        }
    }
}
